package com.example.demo.validator;

import com.example.demo.dto.ProductDto;
import com.example.demo.model.enums.Category;
import org.springframework.stereotype.Component;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

@Component
public class ProductDtoValidator {
    public void validate(ProductDto productDto, BindingResult bindingResult){
        validateName(productDto, bindingResult);
        validatePrice(productDto, bindingResult);
        validateCategory(productDto,bindingResult);
    }

    private void validatePrice(ProductDto productDto, BindingResult bindingResult) {
        try{
            int price = Integer.parseInt(productDto.getPrice());
            if (price<=0){
                FieldError fieldError = new FieldError("productDto", "price", "Price should be positive");
                bindingResult.addError(fieldError);
            }

        } catch (NumberFormatException exception){
            FieldError fieldError = new FieldError("productDto", "price", "Price should be a numerical value");
            bindingResult.addError(fieldError);
        }
    }

    private void validateName(ProductDto productDto, BindingResult bindingResult) {
        if(productDto.getName().length() == 0){
            FieldError fieldError = new FieldError("productDto", "name", "Please input a valid name.");
            bindingResult.addError(fieldError);
        }
    }

    private void validateCategory(ProductDto productDto, BindingResult bindingResult) {
        try{
            Category category = Category.valueOf(productDto.getCategory());
        } catch (IllegalArgumentException exception){
            FieldError fieldError = new FieldError("productDto", "category", "Category isn't valid");
            bindingResult.addError(fieldError);
        }
    }
}
