package com.example.demo.service;

import com.example.demo.dto.ProductDto;
import com.example.demo.dto.QuantityDto;
import com.example.demo.mapper.ProductMapper;
import com.example.demo.model.Product;
import com.example.demo.model.ShoppingCart;
import com.example.demo.repository.ProductRepository;
import com.example.demo.repository.ShoppingCartRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class ShoppingCartService {

    @Autowired
    private ShoppingCartRepository shoppingCartRepository;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private ProductMapper productMapper;

    public boolean addProduct(String productId, QuantityDto quantityDto,String userEmail){
        ShoppingCart shoppingCart = shoppingCartRepository.findByUserEmail(userEmail);
        System.out.println("Am gasit shopping cart-ul cu id-ul: " + shoppingCart.getShoppingCartId());
        Optional<Product> optionalProduct = productRepository.findById(Integer.valueOf(productId));
        if(optionalProduct.isEmpty()){
            System.out.println("optional product is empty");
            return false;
        }
        Product product = optionalProduct.get();
        Integer quantity = Integer.valueOf(quantityDto.getQuantity());
        if(product.getStock().getQuantity()<quantity){
            System.out.println("product is out of stock");
            return false;
        }
        for(int index = 0;index<quantity;index++) {
            shoppingCart.addProduct(product);
        }
        shoppingCartRepository.save(shoppingCart);
        return true;
    }

    public Map<ProductDto, Integer> retrieveShoppingCartContent(String userEmail) {
        ShoppingCart shoppingCart = shoppingCartRepository.findByUserEmail(userEmail);
        List<Product> products = shoppingCart.getProducts();
        Map<ProductDto,Integer> result = new LinkedHashMap<>();
        for(Product product : products){
            ProductDto productDto = productMapper.map(product);
            if (result.containsKey(productDto)){
                Integer numberOfProducts = result.get(productDto);
                result.put(productDto,numberOfProducts+1);
            } else {
            result.put(productDto,1);}
        }
        System.out.println(result);
        return result;
    }
}
