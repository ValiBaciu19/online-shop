package com.example.demo.model.enums;

public enum OrderStatus {
    APPROVED, PENDING, FAILED, PROCESSING
}
